package finki.ukim.mk.emt.rentacar.carrental.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainObjectId;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.ValueObject;

public class RentalId extends DomainObjectId {

    private String id;

    public RentalId(String id) {
        super(id);
    }
}
