package finki.ukim.mk.emt.rentacar.carrental.repositories;

import finki.ukim.mk.emt.rentacar.carrental.domain.Local;
import finki.ukim.mk.emt.rentacar.carrental.domain.LocalId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LocalRepository extends JpaRepository<Local,LocalId> {
}
