package finki.ukim.mk.emt.rentacar.carrental.domain.events;

import com.fasterxml.jackson.annotation.JsonProperty;
import finki.ukim.mk.emt.rentacar.carrental.domain.CarId;
import finki.ukim.mk.emt.rentacar.carrental.domain.RentalId;
import finki.ukim.mk.emt.rentacar.carrental.domain.UserId;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainEvent;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.financial.Currency;
import org.springframework.lang.NonNull;

import java.time.Instant;
import java.time.LocalDate;
import java.util.Objects;

public class CarRentedEvent implements DomainEvent {

    @JsonProperty("rentalId")
    private final RentalId rentalId;

    @JsonProperty("createdWhen")
    private final Instant createdWhen;

    @JsonProperty("customer")
    private final UserId userId;

    @JsonProperty("carId")
    private final CarId carId;

    @JsonProperty("from")
    private final LocalDate from;

    @JsonProperty("to")
    private final LocalDate to;

    @JsonProperty("currency")
    private final Currency currency;

    public CarRentedEvent(@NonNull RentalId rentalId, @NonNull Instant createdWhen, @NonNull UserId userId, @NonNull CarId carId, LocalDate from, LocalDate to, Currency currency) {
        this.rentalId = Objects.requireNonNull(rentalId, "the rental id must not be null");
        this.createdWhen = Objects.requireNonNull(createdWhen, "occurredOn must not be null");
        this.userId = Objects.requireNonNull(userId, "userId must not be null");
        this.carId = Objects.requireNonNull(carId, "userId must not be null");
        this.from = from;
        this.to = to;
        this.currency = currency;
    }

    @NonNull
    public RentalId rentalId() {
        return rentalId;
    }

    @Override
    @NonNull
    public Instant createdWhen() {
        return createdWhen;
    }

    public UserId getUserId() {
        return userId;
    }

    public CarId getCarId() {
        return carId;
    }

    public LocalDate getFrom() {
        return from;
    }

    public LocalDate getTo() {
        return to;
    }

    public Currency getCurrency() {
        return currency;
    }
}
