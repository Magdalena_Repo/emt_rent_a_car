package finki.ukim.mk.emt.rentacar.carrental.repositories;

import finki.ukim.mk.emt.rentacar.carrental.domain.User;
import finki.ukim.mk.emt.rentacar.carrental.domain.UserId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, UserId> {
}
