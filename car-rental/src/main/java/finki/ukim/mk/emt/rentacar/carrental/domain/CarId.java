package finki.ukim.mk.emt.rentacar.carrental.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainObjectId;

import javax.persistence.Embeddable;
import java.io.Serializable;

@Embeddable
public class CarId extends DomainObjectId implements Serializable {
    private String id;
    public CarId(String id) {
        super(id);
    }
}
