package finki.ukim.mk.emt.rentacar.carrental.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainObjectId;

import javax.persistence.Embeddable;

@Embeddable
public class UserId extends DomainObjectId {
    private String id;

    public UserId(String id) { super(id); }
}
