package finki.ukim.mk.emt.rentacar.carrental.domain.events;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import finki.ukim.mk.emt.rentacar.carrental.domain.Local;
import finki.ukim.mk.emt.rentacar.carrental.domain.LocalId;
import finki.ukim.mk.emt.rentacar.carrental.domain.RentalId;
import finki.ukim.mk.emt.rentacar.carrental.domain.UserId;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainEvent;
import org.springframework.lang.NonNull;

import java.time.Instant;
import java.util.Objects;

public class CarLocalCreatedEvent  implements DomainEvent {
    @JsonProperty("rentalId")
    private final RentalId rentalId;

    @JsonProperty("createdWhen")
    private final Instant createdWhen;

    @JsonProperty("ownedBy")
    private final UserId userId;

    @JsonProperty("local")
    private final Local local;

    @JsonCreator
    public CarLocalCreatedEvent(@JsonProperty("rentalId") @NonNull RentalId rentalId,
                                @JsonProperty("occurredOn") @NonNull Instant createdWhen, @NonNull UserId userId, Local local) {
        this.rentalId = Objects.requireNonNull(rentalId, "the rental id must not be null");
        this.createdWhen = Objects.requireNonNull(createdWhen, "occurredOn must not be null");
        this.userId = Objects.requireNonNull(userId, "userId must not be null");
        this.local = local;
    }

    @NonNull
    public RentalId rentalId() {
        return rentalId;
    }

    @Override
    @NonNull
    public Instant createdWhen() {
        return createdWhen;
    }

    public UserId getUserId() {
        return userId;
    }

    public Local getLocal() {
        return local;
    }
}
