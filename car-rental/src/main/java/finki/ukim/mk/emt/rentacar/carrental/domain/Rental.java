package finki.ukim.mk.emt.rentacar.carrental.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.AbstractEntity;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.financial.Currency;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.financial.Money;
import lombok.Getter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;

@Entity
@Table(name = "rentals")
@Getter
public class Rental extends AbstractEntity<RentalId> {

    private LocalDate dateFrom;
    private LocalDate dateTo;
    @Embedded
    private CarId carId;
    @Embedded
    private UserId userId;
    private Long numberOfDays;

    @Embedded
    private Money totalRentalPrice;


    public Rental() {
    }

    public Rental(@NotNull LocalDate dateFrom, @NotNull LocalDate dateTo, @NotNull CarId carId,
                  @NotNull UserId userId, @NotNull Money totalRentalPrice) {
        if(dateFrom.isAfter(dateTo)) {
            throw new IllegalArgumentException("Please enter valid dates.");
        }
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
        this.userId = userId;
        this.carId = carId;
        if(totalRentalPrice.getPricePerDay() < 0 || totalRentalPrice.getPricePerDay() == null) {
            throw new IllegalArgumentException("The price per day for the car with id:" + carId.getId() + " is not correct");
        }
        this.totalRentalPrice = totalRentalPrice;
    }

    @Override
    public RentalId id() {
        return super.id();
    }

    public void setPeriodForRental(@NotNull LocalDate dateFrom, @NotNull LocalDate dateTo) {
        this.dateFrom = dateFrom;
        this.dateTo = dateTo;
    }

    public void setTotalRentalPrice(Money totalRentalPrice) {
        this.totalRentalPrice = totalRentalPrice;
    }
    public Money calculateTotalRentalPrice(Currency currency, Double pricePerDay) {
        Money totalPrice = new Money(pricePerDay, null, currency);
        this.numberOfDays = ChronoUnit.DAYS.between(dateFrom, dateTo);
        totalPrice.calculateTotalPrice(numberOfDays);
        return totalPrice;
    }
    public Car rentACar(Car car) {
        return car.reduceAvailableQuantity(true, this);
    }
}
