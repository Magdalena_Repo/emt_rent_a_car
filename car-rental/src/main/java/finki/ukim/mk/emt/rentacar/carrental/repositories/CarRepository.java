package finki.ukim.mk.emt.rentacar.carrental.repositories;

import finki.ukim.mk.emt.rentacar.carrental.domain.Car;
import finki.ukim.mk.emt.rentacar.carrental.domain.CarId;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface CarRepository extends JpaRepository<Car, CarId> {
}
