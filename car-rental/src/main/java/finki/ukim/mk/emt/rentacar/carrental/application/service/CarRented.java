package finki.ukim.mk.emt.rentacar.carrental.application.service;

import finki.ukim.mk.emt.rentacar.carrental.domain.Car;
import finki.ukim.mk.emt.rentacar.carrental.domain.CarId;
import finki.ukim.mk.emt.rentacar.carrental.domain.Rental;
import finki.ukim.mk.emt.rentacar.carrental.domain.User;
import finki.ukim.mk.emt.rentacar.carrental.domain.events.CarRentedEvent;
import finki.ukim.mk.emt.rentacar.carrental.repositories.CarRepository;
import finki.ukim.mk.emt.rentacar.carrental.repositories.RentalRepository;
import finki.ukim.mk.emt.rentacar.carrental.repositories.UserRepository;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.financial.Currency;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CarRented {


    private final CarRepository carRepository;
    private final UserRepository userRepository;
    private final RentalRepository rentalRepository;

    public CarRented(CarRepository carRepository, UserRepository userRepository, RentalRepository rentalRepository) {
        this.carRepository = carRepository;
        this.userRepository = userRepository;
        this.rentalRepository = rentalRepository;
    }

    public List<Car> findAllCars(){
        return carRepository.findAll();
    }

    public Car findCarById(CarId carId){
        Optional<Car> car =carRepository.findById(carId);
        if(car.isPresent()) {
            return car.get();
        } else {
            throw new IllegalArgumentException("The car does not exist");
        }
    }
    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void onCarRented(CarRentedEvent carRentedEvent){
        Car rentedCar = carRepository.findById(carRentedEvent.getCarId()).orElseThrow( RuntimeException::new);
        User user = userRepository.findById(carRentedEvent.getUserId()).orElseThrow( RuntimeException::new);
        Rental rental= new Rental();
//        LocalDate from =  LocalDate.of(2020,3,5);
//        LocalDate to = LocalDate.now();
        rental.setPeriodForRental(carRentedEvent.getFrom(),carRentedEvent.getTo());
        Currency currency = carRentedEvent.getCurrency();

        rental.calculateTotalRentalPrice(currency,rentedCar.getPriceforCar().getPricePerDay());
        rentalRepository.save(rental);
        rentedCar.reduceAvailableQuantity();
        carRepository.save(rentedCar);
    }

}
