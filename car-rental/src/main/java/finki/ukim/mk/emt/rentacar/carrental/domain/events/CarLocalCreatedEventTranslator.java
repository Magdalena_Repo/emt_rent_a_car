package finki.ukim.mk.emt.rentacar.carrental.domain.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainEvent;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.RemoteEventTranslator;
import finki.ukim.mk.emt.rentacar.sharedmodule.logs.StoredDomainEvent;

import java.util.Optional;

public class CarLocalCreatedEventTranslator implements RemoteEventTranslator {

    private final ObjectMapper objectMapper;


    public CarLocalCreatedEventTranslator(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public boolean supports(StoredDomainEvent storedDomainEvent) {
        return storedDomainEvent
                .domainEventClassName().equals("finki.ukim.mk.emt.rentacar.carrental.domain.events.CarLocalCreatedEvent");
    }

    @Override
    public Optional<DomainEvent> translate(StoredDomainEvent remoteEvent) {
        return Optional.of(remoteEvent.toDomainEvent(objectMapper,CarLocalCreatedEvent.class));
    }
}
