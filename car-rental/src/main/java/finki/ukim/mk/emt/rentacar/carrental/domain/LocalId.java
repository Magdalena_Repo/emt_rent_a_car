package finki.ukim.mk.emt.rentacar.carrental.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainObjectId;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;

@Embeddable
public class LocalId  extends DomainObjectId {
    private String id;

    public LocalId(String id) {
        super(id);
    }
}
