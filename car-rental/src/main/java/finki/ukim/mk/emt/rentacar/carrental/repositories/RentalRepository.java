package finki.ukim.mk.emt.rentacar.carrental.repositories;

import finki.ukim.mk.emt.rentacar.carrental.domain.Rental;
import finki.ukim.mk.emt.rentacar.carrental.domain.RentalId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RentalRepository extends JpaRepository<Rental,RentalId> {
}
