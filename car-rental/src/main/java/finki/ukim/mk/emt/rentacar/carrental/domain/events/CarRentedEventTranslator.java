package finki.ukim.mk.emt.rentacar.carrental.domain.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainEvent;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.RemoteEventTranslator;
import finki.ukim.mk.emt.rentacar.sharedmodule.logs.StoredDomainEvent;

import java.util.Optional;

public class CarRentedEventTranslator implements RemoteEventTranslator {

    private final ObjectMapper objectMapper;

    public CarRentedEventTranslator(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    @Override
    public boolean supports(StoredDomainEvent storedDomainEvent) {
        return storedDomainEvent
                .domainEventClassName().equals("finki.ukim.mk.emt.rentacar.carrental.domain.events.CarRentedEvent");
    }

    @Override
    public Optional<DomainEvent> translate(StoredDomainEvent remoteEvent) {
        return Optional.of(remoteEvent.toDomainEvent(objectMapper,CarRentedEvent.class));
    }
}
