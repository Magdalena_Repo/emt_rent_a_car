package finki.ukim.mk.emt.rentacar.carrental.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.local_based_domain.Address;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.local_based_domain.Coordinates;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.local_based_domain.Country;
import lombok.Getter;

import javax.persistence.Embeddable;
import javax.persistence.Embedded;
import javax.validation.constraints.NotNull;
import java.util.Objects;

@Embeddable
@Getter
public class Local extends Address{

    private String email;
    private String name;
    private String phoneNumber;
    @Embedded
    private UserId ownerId;

    public Local() {
    }

    public Local(@NotNull UserId ownerId, String address, Coordinates coordinates, Country country, String email, String name, String phoneNumber) {
        super(address, coordinates, country);
        this.ownerId = ownerId;
        this.email = email;
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Local)) return false;
        if (!super.equals(o)) return false;
        Local local = (Local) o;
        return Objects.equals(email, local.email) &&
                Objects.equals(name, local.name) &&
                Objects.equals(phoneNumber, local.phoneNumber) &&
                Objects.equals(ownerId, local.ownerId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), email, name, phoneNumber, ownerId);
    }

    @Override
    public String toString() {
        return "Local{" +
                "email='" + email + '\'' +
                ", name='" + name + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                '}';
    }
}
