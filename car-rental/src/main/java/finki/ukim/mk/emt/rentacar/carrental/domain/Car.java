package finki.ukim.mk.emt.rentacar.carrental.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.AbstractEntity;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.financial.Money;
import lombok.Getter;
import org.hibernate.annotations.Where;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;


@Entity
@Getter
@Table(name = "car")
@Where(clause = "deleted=false")
public class Car extends AbstractEntity<CarId> {

    @EmbeddedId
    private CarId id;

    private String model;
    private LocalDate dateOfProduction;
    private String typeOfFuel;
    private String carBrand;
    @Embedded
    private Money priceforCar;
    @Column(name = "deleted", nullable = false)
    private boolean deleted = false;
    @Embedded
    private Local rentedIn;
    private Long availableQuantity;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private Set<Rental> rentals = new HashSet<>();

    public CarId getCarId() {
        return id;
    }

    public Car() {
    }

    public Car(@NotNull CarId id, String model, LocalDate dateOfProduction, String typeOfFuel, String carBrand,
               Money priceforCar, boolean deleted, Local rentedIn, Long availableQuantity) {
        this.id = id;
        this.model = model;
        this.dateOfProduction = dateOfProduction;
        this.typeOfFuel = typeOfFuel;
        this.carBrand = carBrand;
        this.priceforCar = priceforCar;
        this.deleted = deleted;
        this.rentedIn = rentedIn;
        this.availableQuantity = availableQuantity;
    }

    public Car reduceAvailableQuantity(boolean carRented, Rental rental) {
        if(carRented && this.availableQuantity > 0) {
            this.availableQuantity --;
            this.rentals.add(rental);
        } else {
            throw new IllegalArgumentException("You can not rent this car it's out of stock");
        }
        return this;
    }
    public void reduceAvailableQuantity(){
        this.availableQuantity--;
    }
}
