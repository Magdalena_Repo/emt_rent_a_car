package finki.ukim.mk.emt.rentacar.carrental.application.service;

import finki.ukim.mk.emt.rentacar.carrental.domain.*;
import finki.ukim.mk.emt.rentacar.carrental.domain.events.CarLocalCreatedEvent;
import finki.ukim.mk.emt.rentacar.carrental.domain.events.CarRentedEvent;
import finki.ukim.mk.emt.rentacar.carrental.repositories.LocalRepository;
import finki.ukim.mk.emt.rentacar.carrental.repositories.UserRepository;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.financial.Currency;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.local_based_domain.Coordinates;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.local_based_domain.Country;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public class LocalCreated {

    private final UserRepository userRepository;
    private final LocalRepository localRepository;


    public LocalCreated(UserRepository userRepository, LocalRepository localRepository) {
        this.userRepository = userRepository;
        this.localRepository = localRepository;
    }

    public List<Local> findAllLocals(){
        return localRepository.findAll();
    }

    public Local findLocalBydId(LocalId localid){
        Optional<Local> local = localRepository.findById(localid);
        if(local.isPresent()) {
            return local.get();
        } else {
            throw new IllegalArgumentException("The local does not exist");
        }
    }
    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void createLocal(CarLocalCreatedEvent localCreatedEvent){
        User user = userRepository.findById(localCreatedEvent.getUserId()).orElseThrow( RuntimeException::new);
//        Local local = new Local(localCreatedEvent.getUserId(), "Bul.Jane Sandanski", new Coordinates(33.6,666.9)
//                , Country.HR, "myEmail@gmail.com", "First Owner", "+38966655");
        Local local = new Local(localCreatedEvent.getLocal().getOwnerId(),localCreatedEvent.getLocal().address(), localCreatedEvent.getLocal().city()
                ,localCreatedEvent.getLocal().country(),localCreatedEvent.getLocal().getEmail(),localCreatedEvent.getLocal().getName(),localCreatedEvent.getLocal().getPhoneNumber());
        localRepository.save(local);
    }

}
