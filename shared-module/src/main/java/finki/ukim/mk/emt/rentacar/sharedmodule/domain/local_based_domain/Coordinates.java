package finki.ukim.mk.emt.rentacar.sharedmodule.domain.local_based_domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.ValueObject;
import org.springframework.lang.NonNull;

import javax.persistence.Column;
import java.util.Objects;

public class Coordinates implements ValueObject {

    @Column(name="latitude")
    private Double latitude;
    @Column(name="longitude")
    private Double longitude;

    public Coordinates() { }

    public Coordinates(Double latitude, Double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }
    @NonNull
    @JsonProperty("latitude")
    public Double latitude() {
        return latitude;
    }

    @NonNull
    @JsonProperty("longitude")
    public Double longitude() {
        return longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Coordinates)) return false;
        Coordinates that = (Coordinates) o;
        return Objects.equals(latitude, that.latitude) &&
                Objects.equals(longitude, that.longitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude);
    }

    @Override
    public String toString() {
        return "Coordinates{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
