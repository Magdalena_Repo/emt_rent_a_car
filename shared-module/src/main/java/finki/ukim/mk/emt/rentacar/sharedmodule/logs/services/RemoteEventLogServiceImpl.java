package finki.ukim.mk.emt.rentacar.sharedmodule.logs.services;

import finki.ukim.mk.emt.rentacar.sharedmodule.logs.StoredDomainEvent;
import lombok.NonNull;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.SimpleClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.RemoteEventLog;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.List;


@Service
public class RemoteEventLogServiceImpl implements RemoteEventLogService {

    private final String sourceModule;
    private final String sourceModuleUrl;
    private final RestTemplate restTemplate;

    public RemoteEventLogServiceImpl(@NonNull String sourceModule, @NonNull String sourceModuleUrl,
                                     int connectionTimeOut, int readTimeOut) {
        this.sourceModule = sourceModule;
        this.sourceModuleUrl = sourceModuleUrl;
        this.restTemplate = new RestTemplate();
        var requestFactory = new SimpleClientHttpRequestFactory();

        requestFactory.setConnectTimeout(connectionTimeOut);
        requestFactory.setReadTimeout(readTimeOut);
        restTemplate.setRequestFactory(requestFactory);
    }


    @Override
    public String source() {
        return sourceModule;
    }

    @Override
    public RemoteEventLog currentLog(long lastProcessedId) {
        URI toUri = UriComponentsBuilder.fromUriString(sourceModuleUrl)
                .path(String.format("/api/v1/event-logs", lastProcessedId)).build().toUri();

        return retrieveEventLog(toUri);
    }

    private RemoteEventLog retrieveEventLog(@NonNull URI uri) {
        ResponseEntity<List<StoredDomainEvent>> listOfStoredDomainEvents
                = restTemplate.exchange(uri, HttpMethod.GET,
                null, new ParameterizedTypeReference<List<StoredDomainEvent>>() {});
        if(listOfStoredDomainEvents.getStatusCode() != HttpStatus.OK) {
            throw  new RuntimeException("An error occurred the log has not been retrieved");
        }
        return null; //TODO fix this!
    }
}
