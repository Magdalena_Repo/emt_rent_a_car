package finki.ukim.mk.emt.rentacar.sharedmodule.domain.financial;

public enum Currency {
    MKD,
    EUR,
    USD,
    GBP
}
