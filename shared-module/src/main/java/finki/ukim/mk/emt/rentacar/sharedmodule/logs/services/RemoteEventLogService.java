package finki.ukim.mk.emt.rentacar.sharedmodule.logs.services;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.RemoteEventLog;

public interface RemoteEventLogService {

    String source();

    RemoteEventLog currentLog(long lastProcessedId);
}
