package finki.ukim.mk.emt.rentacar.sharedmodule.domain.local_based_domain;

public enum Country {
    MK,
    SRB,
    BG,
    GR,
    KS,
    AL,
    BIS,
    MON,
    SL,
    HR
}
