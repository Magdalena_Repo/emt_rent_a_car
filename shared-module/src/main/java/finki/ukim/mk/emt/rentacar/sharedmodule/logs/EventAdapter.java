package finki.ukim.mk.emt.rentacar.sharedmodule.logs;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainEvent;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

@Service

public class EventAdapter {
    private final EventLogService eventLogService;

    public EventAdapter(EventLogService eventLogService) {
        this.eventLogService = eventLogService;
    }

    @TransactionalEventListener(phase = TransactionPhase.BEFORE_COMMIT)
    public void onDomainEvent(@NonNull DomainEvent domainEvent) {
        eventLogService.appendNewLog(domainEvent);
    }
}
