package finki.ukim.mk.emt.rentacar.sharedmodule.domain.financial;

import com.sun.istack.NotNull;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.ValueObject;
import lombok.Getter;
import lombok.ToString;

import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.Objects;


@Getter
@ToString
@Embeddable
public class Money implements ValueObject {

    private final Double pricePerDay;
    private final Double totalPrice;
    @Enumerated(value = EnumType.STRING)
    private final Currency currency;


    public Money(@NotNull Double pricePerDay, Double totalPrice, @NotNull Currency currency) {
        this.pricePerDay = pricePerDay;
        this.totalPrice = totalPrice;
        this.currency = currency;
    }

    public static Money setPricePerDay(Double pricePrerDay,Currency currency) {
        return new Money(pricePrerDay, null, currency);
    }

    public Money calculatePricePerDay(Money money){
        if(!money.currency.equals(currency)){
            throw  new IllegalArgumentException("Different currencies.");
        }
        return new Money(pricePerDay + money.pricePerDay, totalPrice, currency);
    }
    public Money substractMoney(Money money){
        if(!money.currency.equals(currency)){
            throw  new IllegalArgumentException("Different currencies.");
        }
        return new Money(pricePerDay - money.pricePerDay, totalPrice, currency);
    }

    public Money calculateTotalPrice(Long numberOfDays){
        if(numberOfDays == 0){
            return new Money(pricePerDay, pricePerDay, currency);
        }
        return new Money(pricePerDay, pricePerDay * numberOfDays , currency);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Money)) return false;
        Money money = (Money) o;
        return Objects.equals(pricePerDay, money.pricePerDay) &&
                currency == money.currency;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pricePerDay, currency);
    }
}
