package finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.logs.StoredDomainEvent;

import java.util.List;

public interface RemoteEventLog {

    List<StoredDomainEvent> events();
}
