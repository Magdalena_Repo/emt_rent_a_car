package finki.ukim.mk.emt.rentacar.sharedmodule.logs.services;


import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.ProcessedRemoteEvent;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.RemoteEventTranslator;
import finki.ukim.mk.emt.rentacar.sharedmodule.logs.StoredDomainEvent;
import finki.ukim.mk.emt.rentacar.sharedmodule.logs.repositories.ProcessedRemoteEventRepository;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;

import java.util.List;
import java.util.Map;

@Service
public class RemoteEventProcessor {

    private final ProcessedRemoteEventRepository processedRemoteEventRepository;
    private final Map<String, RemoteEventLogService> remoteEventLogs;
    private final Map<String, RemoteEventTranslator> remoteEventTranslators;
    private final ApplicationEventPublisher applicationEventPublisher;
    private final TransactionTemplate transactionTemplate;

    public RemoteEventProcessor(ProcessedRemoteEventRepository processedRemoteEventRepository, Map<String, RemoteEventLogService> remoteEventLogs, Map<String, RemoteEventTranslator> remoteEventTranslatos,
                                ApplicationEventPublisher applicationEventPublisher, TransactionTemplate transactionTemplate) {
        this.processedRemoteEventRepository = processedRemoteEventRepository;
        this.remoteEventLogs = remoteEventLogs;
        this.remoteEventTranslators = remoteEventTranslatos;
        this.applicationEventPublisher = applicationEventPublisher;
        this.transactionTemplate = transactionTemplate;
    }

    @Scheduled(fixedDelay = 60000)
    public void processEvents() {
        remoteEventLogs.values().forEach(this::processEvents);
    }

    private void processEvents(RemoteEventLogService remoteEventLogService) {

        var log = remoteEventLogService.currentLog(getLastProcessedId(remoteEventLogService));

        processEvents(remoteEventLogService, log.events());

    }

    private long getLastProcessedId(RemoteEventLogService remoteEventLogService) {
        return processedRemoteEventRepository.findById(remoteEventLogService.source())
                .map(x -> x.lastProcessedEventId())
                .orElse(0L);
    }

    private void processEvents(RemoteEventLogService remoteEventLogService, List<StoredDomainEvent> events) {
        events.forEach(event -> {
            transactionTemplate.execute(new TransactionCallbackWithoutResult() {
                @Override
                protected void doInTransactionWithoutResult(TransactionStatus status) {
                    publishEvent(event);
                    setLastProcessedId(remoteEventLogService, event.id());
                }
            });
        });
    }

    private void setLastProcessedId(RemoteEventLogService remoteEventLogService, long lastProcessedId) {
        processedRemoteEventRepository.saveAndFlush(new ProcessedRemoteEvent(remoteEventLogService.source(), lastProcessedId));
    }

    private void publishEvent(StoredDomainEvent event) {
        remoteEventTranslators.values().stream()
                .filter(translator -> translator.supports(event))
                .findFirst()
                .flatMap(translator -> translator.translate(event))
                .ifPresent(applicationEventPublisher::publishEvent);
    }

}
