package finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain;


import org.springframework.lang.Nullable;

import java.io.Serializable;

public interface IdentifiableDomainObject<ID extends Serializable> extends DomainObject  {

    @Nullable
    ID id();
}
