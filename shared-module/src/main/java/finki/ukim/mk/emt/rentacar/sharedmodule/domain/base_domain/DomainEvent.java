package finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.lang.NonNull;

import java.time.Instant;

public interface DomainEvent extends DomainObject{
    @NonNull
    Instant createdWhen();
}
