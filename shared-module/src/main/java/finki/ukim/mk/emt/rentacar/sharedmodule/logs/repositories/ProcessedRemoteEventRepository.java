package finki.ukim.mk.emt.rentacar.sharedmodule.logs.repositories;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.ProcessedRemoteEvent;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProcessedRemoteEventRepository extends JpaRepository<ProcessedRemoteEvent,String> {
}
