package finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain;

import lombok.ToString;

import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.util.Objects;

@ToString
@MappedSuperclass
public abstract class AbstractEntity<ID extends DomainObjectId & Serializable> implements IdentifiableDomainObject<ID> {

    @Id
    protected ID id;
    public AbstractEntity() { }

    public void AbstractEntity(ID id)
    {
        this.id=id;
    }
    @Override
    public ID id() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AbstractEntity)) return false;
        AbstractEntity<?> that = (AbstractEntity<?>) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
