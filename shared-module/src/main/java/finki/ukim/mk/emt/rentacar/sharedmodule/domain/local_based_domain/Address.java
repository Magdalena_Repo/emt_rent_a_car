package finki.ukim.mk.emt.rentacar.sharedmodule.domain.local_based_domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.ValueObject;
import org.springframework.lang.NonNull;

import javax.persistence.*;
import java.util.Objects;

@Embeddable
@MappedSuperclass
public class Address implements ValueObject {

    @Column(name = "address")
    private String address;

    @Column(name = "coordinates")
    @Embedded
    private Coordinates coordinates;

    @Column(name = "country")
    @Enumerated(EnumType.STRING)
    private Country country;


    @SuppressWarnings("unused") // Used by JPA only.
    protected Address() {
    }

    public Address(@NonNull String address, @NonNull Coordinates coordinates,
                   @NonNull Country country) {
        this.address = address;
        this.coordinates = coordinates;
        this.country = country;
    }

    @NonNull
    @JsonProperty("address")
    public String address() {
        return address;
    }

    @NonNull
    @JsonProperty("coordinates")
    public Coordinates city() {
        return coordinates;
    }

    @NonNull
    @JsonProperty("country")
    public Country country() {
        return country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Address)) return false;
        Address address1 = (Address) o;
        return Objects.equals(address, address1.address) &&
                Objects.equals(coordinates, address1.coordinates) &&
                country == address1.country;
    }

    @Override
    public int hashCode() {
        return Objects.hash(address, coordinates, country);
    }

    @Override
    public String toString() {
        return "Address{" +
                "address='" + address + '\'' +
                ", coordinates=" + coordinates +
                ", country=" + country +
                '}';
    }
}

