package finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.logs.StoredDomainEvent;

import java.util.Optional;

public interface RemoteEventTranslator {

    boolean supports(StoredDomainEvent storedDomainEvent);

    Optional<DomainEvent> translate(StoredDomainEvent remoteEvent);
}
