package finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain;

import org.springframework.lang.NonNull;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;



// Entity for storing the current logs from the events fired.

@Entity
@Table(name = "stored_processed_events")
public
class ProcessedRemoteEvent implements IdentifiableDomainObject<String> {

    @Id
    @Column(name = "module_source", nullable = false)
    private String source;

    @Column(name = "last_event_id", nullable = false)
    private long lastProcessedEventId;

    @SuppressWarnings("unused") // Used by JPA only
    private ProcessedRemoteEvent() {
    }

    public ProcessedRemoteEvent(String source, long lastProcessedEventId) {
        this.source = Objects.requireNonNull(source);
        this.lastProcessedEventId = lastProcessedEventId;
    }

    @NonNull
    public String source() {
        return source;
    }

    @NonNull
    public long lastProcessedEventId() {
        return lastProcessedEventId;
    }

    @Override
    public String id() {
        return source();
    }
}
