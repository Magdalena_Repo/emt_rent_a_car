package finki.ukim.mk.emt.rentacar.sharedmodule.logs;

import com.fasterxml.jackson.databind.ObjectMapper;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainEvent;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Service
public class EventLogService {
    private final StoredDomainEventRepository storedDomainEventRepository;
    private final ObjectMapper objectMapper;

    EventLogService(StoredDomainEventRepository storedDomainEventRepository,
                    ObjectMapper objectMapper) {
        this.storedDomainEventRepository = storedDomainEventRepository;
        this.objectMapper = objectMapper;
    }
    //TODO Remember this, the event is sent only if the transaction is successfully done!
    // It has to be in te same transaction as the modification.
    // All the events in the event publisher are sent in the same transaction! otherwise NO CONSISTANSY

    @Transactional(propagation = Propagation.MANDATORY)
    public void appendNewLog(@NonNull DomainEvent domainEvent) {
        var storedEvent = new StoredDomainEvent(domainEvent, objectMapper);
        storedDomainEventRepository.saveAndFlush(storedEvent);
    }

    @NonNull
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public List<StoredDomainEvent> getLogs(long lastProcessedEventId) {
        var max = storedDomainEventRepository.findHighestDomainEventId();
        if (max == null) {
            max = 0L;
        }
        return storedDomainEventRepository.findEventsBetween(lastProcessedEventId,max);
    }
}
