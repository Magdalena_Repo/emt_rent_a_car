package finki.ukim.mk.emt.rentacar.userauthorization.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainObject;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainObjectId;

public class UserId extends DomainObjectId {

    private String id;

    public UserId(String id) {
        super(id);
    }
}
