package finki.ukim.mk.emt.rentacar.userauthorization.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.AbstractEntity;
import lombok.Getter;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "user")
@Getter
public class User extends AbstractEntity<UserId> {

    private String email;
    private String password;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;
    private String PhoneNumber;

    @Embedded
    private RoleId roleId;

    public User() {
    }

    public User(String email, String password, String firstName, String lastName, LocalDate dateOfBirth, String phoneNumber) {
        this.email = email;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dateOfBirth = dateOfBirth;
        PhoneNumber = phoneNumber;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    @Override
    public UserId id() {
        return super.id();
    }

    public Boolean canCreateLocal() {
        return roleId.getId().equals("1");
    }

    public Boolean canRentACar() {
        return roleId.getId().equals("2");
    }

}
