package finki.ukim.mk.emt.rentacar.userauthorization.domain;

import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainObject;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.DomainObjectId;

public class RoleId extends DomainObjectId {

    private String id;

    public RoleId(String id) {
        super(id);
    }
}
