package finki.ukim.mk.emt.rentacar.userauthorization.repositories;

import finki.ukim.mk.emt.rentacar.userauthorization.domain.Role;
import finki.ukim.mk.emt.rentacar.userauthorization.domain.RoleId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role,RoleId> {
}
