package finki.ukim.mk.emt.rentacar.userauthorization.repositories;

import finki.ukim.mk.emt.rentacar.userauthorization.domain.User;
import finki.ukim.mk.emt.rentacar.userauthorization.domain.UserId;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User,UserId> {
}
