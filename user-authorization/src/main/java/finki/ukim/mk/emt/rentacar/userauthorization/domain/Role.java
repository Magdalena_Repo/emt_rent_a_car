package finki.ukim.mk.emt.rentacar.userauthorization.domain;

import com.sun.istack.NotNull;
import finki.ukim.mk.emt.rentacar.sharedmodule.domain.base_domain.AbstractEntity;
import lombok.Getter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "user_role")
@Getter
@Embeddable
public class Role extends AbstractEntity<RoleId> {

    @Column(name = "user_role")
    private String role;

    @Column(name = "role_description")
    private String description;

    public Role () {

    }

    @Override
    public RoleId id() {
        return super.id();
    }

    public Role(@NotNull String role, String description) {
        this.role = role;
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Role)) return false;
        if (!super.equals(o)) return false;
        Role role1 = (Role) o;
        return Objects.equals(role, role1.role) &&
                Objects.equals(description, role1.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), role, description);
    }
}
